/** @type {import('tailwindcss').Config} */
export default {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {},
    colors: {
      // color palette found here: https://mycolor.space/?hex=%233F4059&sub=1
      main: "#3F4059",
      "ep-white": "#F9F8FF",
      "ep-blue": "#7689F3",
      "ep-glass": "rgba(255, 255, 255, 0.19)",
      "ep-gray": "#AAAABC",
      "ep-red": "#BD6A84",
      "ep-dark-gray": "#454555",
      "ep-orange": "#F28A79",
      "ep-green": "#5BECC0",
    },
  },
  plugins: ["prettier-plugin-tailwindcss"],
};
