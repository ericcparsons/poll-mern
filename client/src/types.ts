//structure of a poll living in the db
export interface IPoll {
  _id: string;
  title: string;
  options: IOption[];
  description: string;
  status: string;
}

//structure of a poll voting option
export interface IOption {
  title: string;
  votes: number;
}

export const COLORS = {
  main: "#3F4059",
  "ep-white": "#F9F8FF",
  "ep-blue": "#7689F3",
  "ep-glass": "rgba(255, 255, 255, 0.19)",
  "ep-gray": "#AAAABC",
  "ep-red": "#BD6A84",
  "ep-dark-gray": "#454555",
  "ep-orange": "#F28A79",
  "ep-green": "#5BECC0",
};
