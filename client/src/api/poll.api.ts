import axios, { AxiosResponse } from "axios";
import { IPoll } from "../types";

export class PollApi {
  private baseUrl: string = "https://poll-mern.onrender.com";
  private apiPath: string = "/api/v1/polls";
  private static instance: PollApi;

  private constructor() {}

  public async getAll(): Promise<IPoll[]> {
    try {
      const resp: AxiosResponse<IPoll[]> = await axios.get(
        `${this.baseUrl}${this.apiPath}`
      );
      return resp.data;
    } catch (error: any) {
      throw new Error(error);
    }
  }

  public async getPoll(id: string): Promise<AxiosResponse<IPoll>> {
    try {
      const poll: AxiosResponse<IPoll> = await axios.get(
        `${this.baseUrl}${this.apiPath}/${id}`
      );
      return poll;
    } catch (error: any) {
      throw new Error(error);
    }
  }

  public async addPoll(formData: IPoll): Promise<AxiosResponse<IPoll>> {
    try {
      const poll: Omit<IPoll, "_id"> = {
        title: formData.title,
        description: formData.description,
        options: formData.options,
        status: formData.status,
      };
      const savePoll: AxiosResponse = await axios.post(
        this.baseUrl + this.apiPath,
        poll
      );
      return savePoll;
    } catch (error: any) {
      throw new Error(error);
    }
  }

  public async updatePoll(poll: IPoll) {
    try {
      const updatedPoll: AxiosResponse<IPoll> = await axios.put(
        `${this.baseUrl}${this.apiPath}/${poll._id}`,
        { poll: poll }
      );
      return updatedPoll;
    } catch (error: any) {
      throw new Error(error);
    }
  }

  public async incPollOptionVote(
    pollId: string,
    optionTitleToInc: string
  ): Promise<AxiosResponse<IPoll>> {
    try {
      //send to ...8082/api/v1/polls/option/:id?optionId
      const updatedPoll: AxiosResponse<IPoll> = await axios.put(
        `${this.baseUrl}${this.apiPath}/option/${pollId}`,
        null,
        { params: { optionTitle: optionTitleToInc } }
      );
      return updatedPoll;
    } catch (error: any) {
      console.log(error.message);
      throw new Error(error);
    }
  }

  public async deletePoll(_id: string): Promise<AxiosResponse<IPoll>> {
    try {
      const deletedPoll: AxiosResponse<IPoll> = await axios.delete(
        `${this.baseUrl}${this.apiPath}/${_id}`
      );
      return deletedPoll;
    } catch (error: any) {
      throw new Error(error);
    }
  }

  public static getInstance() {
    if (!(this.instance instanceof PollApi)) {
      this.instance = new PollApi();
    }

    return this.instance;
  }
}
