import { BrowserRouter, Link, Route, Routes } from "react-router-dom";
import { Polls } from "./Polls/Polls";
import { NewEditPoll } from "./components/newEditPoll/NewEditPoll";
import "@mantine/core/styles.css";
import { MantineProvider, Button } from "@mantine/core";
import { IconPlus, IconChartBar } from "@tabler/icons-react";
import { COLORS } from "./types";

function App() {
  return (
    <MantineProvider>
      <BrowserRouter>
        <div className="flex flex-col h-[100vh]">
          <div className="h-fit bg-main flex items-center shadow-lg justify-between py-5 px-10 font-medium">
            <Link to="/">
              <div className="flex items-center gap-4">
                <IconChartBar className="text-ep-white" size={64} />
                <h1 className="text-4xl sm:text-6xl  text-ep-white font-bold">
                  polls
                </h1>
              </div>
            </Link>
            <Link to={`/new/poll`}>
              <Button
                size="md"
                color={COLORS["ep-green"]}
                leftSection={<IconPlus />}
              >
                New
              </Button>
            </Link>
          </div>
          <div className="flex overflow-y-scroll bg-ep-white flex-col h-full grow">
            <Routes>
              <Route index element={<Polls />} />
              <Route path="/edit/*" element={<NewEditPoll />} />
              <Route path="/new/*" element={<NewEditPoll newPoll={true} />} />
            </Routes>
          </div>
        </div>
      </BrowserRouter>
    </MantineProvider>
  );
}

export default App;
