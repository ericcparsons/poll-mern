import { useEffect, useState } from "react";
import { PollApi } from "../api/poll.api";
import { PollCard } from "../components/pollCard/PollCard";
import { Loader } from "@mantine/core";
import "./Polls.css";
import { COLORS, IPoll } from "../types";

export const Polls = () => {
  const pollApi = PollApi.getInstance();
  const [polls, setPolls] = useState<IPoll[]>([]);
  const [loading, setLoading] = useState(false);
  const [showLoadingMessage, setShowLoadingMessage] = useState(false);

  const getPolls = async (slowMessageId?: number) => {
    try {
      await pollApi.getAll().then((data: IPoll[]) => {
        setPolls(data);
        clearTimeout(slowMessageId);
        setShowLoadingMessage(false);
        setLoading(false);
      });
    } catch (e: unknown) {
      console.error("Unable to retrieve list of polls", e);
    }
  };

  useEffect(() => {
    void (async () => {
      setLoading(true);
      //if loading takes more than 10 secs, show disclaimer about slow server
      const id = setTimeout(() => setShowLoadingMessage(true), 10000);
      await getPolls(id);
    })();
  }, []);
  return (
    <div className="flex flex-col px-5 py-4 justify-start h-full items-center overflow-y-scroll">
      {loading ? (
        <div className="flex flex-col gap-4 justify-center items-center my-auto">
          <Loader className="" size={86} color={COLORS["ep-blue"]} />
          {showLoadingMessage && (
            <div className="text-lg italic font-medium text-ep-blue text-center">
              **Loading times may be slow due to low hobby server activity 🥱
              <br /> requests may be delayed by 50 seconds or more
            </div>
          )}
        </div>
      ) : (
        polls.map((poll: any) => {
          return <PollCard refresh={getPolls} poll={poll} key={poll._id} />;
        })
      )}
    </div>
  );
};
