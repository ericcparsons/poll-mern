import React from "react";
import { Radio } from "@mantine/core";
import { Progress } from "@mantine/core";
import { IOption } from "../../types";
interface PollOptionProps {
  setSelectedOption: (value: string) => any;
  pollId: string;
  option: IOption;
  totalVotes: number;
  voted: boolean;
}

export const PollOption = ({
  setSelectedOption,
  pollId,
  option,
  totalVotes,
  voted,
}: PollOptionProps) => {
  /**
   * use the passed in props callback to update the current selected value in the component state
   * @param event: DOM event
   */
  const setOptionVote = (event: React.BaseSyntheticEvent) => {
    setSelectedOption(event.target.value);
  };

  /**
   * helper to calculate the options number votes as a total percentage of all votes
   * @param option
   * @returns
   */
  const getPercentageOfVotes = (option: IOption) =>
    Math.round((option.votes / totalVotes) * 100);

  return (
    <div className="my-1 flex gap-3">
      {!voted ? (
        <Radio
          onChange={setOptionVote}
          name={`${pollId}`}
          label={option.title}
          value={option.title}
        />
      ) : (
        <div className="w-full">
          {option.title}
          <Progress.Root size="xl">
            <Progress.Section animated value={getPercentageOfVotes(option)}>
              <Progress.Label>{`${getPercentageOfVotes(option)}% (${option.votes} votes)`}</Progress.Label>
            </Progress.Section>
          </Progress.Root>
        </div>
      )}
    </div>
  );
};
