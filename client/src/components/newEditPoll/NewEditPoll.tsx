import { Button, Loader, TextInput, Textarea } from "@mantine/core";
import { useForm } from "@mantine/form";
import { useEffect, useState } from "react";
import { Link, useNavigate, useSearchParams } from "react-router-dom";
import { PollApi } from "../../api/poll.api";
import { COLORS, IPoll } from "../../types";
import { IconArrowLeft, IconX, IconPlus, IconCheck } from "@tabler/icons-react";
interface NewEditPollProps {
  newPoll?: boolean;
}

//default value for currentPoll state
const emptyPoll: IPoll = {
  title: "",
  description: "",
  _id: "",
  status: "",
  // polls should all have at least 2 options, no less
  options: [
    { title: "", votes: 0 },
    { title: "", votes: 0 },
  ],
};

/**
 *
 * @param newPoll : if "true" we're making a new poll, false, we're editing a poll
 * @returns
 */
export const NewEditPoll = ({ newPoll = false }: NewEditPollProps) => {
  const navigate = useNavigate();

  const [loading, setLoading] = useState(false);

  //used to get the current poll id from the url query params (only used in edit poll)
  const [searchParams] = useSearchParams();
  //holds the current poll the form is either editing or creating
  const [currentPoll, setCurrentPoll] = useState<IPoll>(emptyPoll);
  const pollApi = PollApi.getInstance();

  const form = useForm({
    mode: "uncontrolled",
    initialValues: {
      title: currentPoll.title,
      description: currentPoll.description,
      options: currentPoll.options,
    },
    validate: {
      title: (value) => (value.length === 0 ? "Title must not be empty" : null),
      description: (value) =>
        value.length === 0 ? "Description must not be empty" : null,
      options: {
        title: (value) =>
          value.length === 0 ? "Option title must not be empty" : null,
      },
    },
  });

  /**
   * handling for adding an additional option to the form and the current poll
   * so that an object exists to store any new values in said new input field
   */
  const addOptionFormFields = () => {
    const currentForm = form.getValues();
    currentForm.options.push({ title: "", votes: 0 });
    setCurrentPoll({ ...currentPoll, ...form.getValues() });
    form.setValues(currentForm);
  };

  /**
   * handling for getting rid of a new/existing poll vote option on click of the "Remove" button
   * @param i: index in the options array that the option we want to remove is at
   */
  const removeOptionFormFields = (i: number) => {
    let newPollValues = { ...currentPoll };
    newPollValues.options.splice(i, 1);
    setCurrentPoll(newPollValues);
  };

  /**
   * when in edit mode, get the poll from the id in query params in the url, by pinging the backend API
   */
  const getPollFromAPI = async () => {
    setLoading(true);
    const id = searchParams.get("id");
    if (id) {
      await pollApi
        .getPoll(id)
        .then((res) => {
          const poll: IPoll = res.data;
          setCurrentPoll(poll);
          form.initialize(poll);
        })
        .catch((e: Error) => {
          throw new Error(e.message);
        });
    }
    setLoading(false);
  };

  /**
   * onSubmit of the form (in new mode), create a new poll in the db
   * upon success, redirect back to the list of polls
   * @param event
   */
  const save = async () => {
    const updatedPoll = { ...currentPoll, ...form.getValues() };
    setCurrentPoll(updatedPoll);
    try {
      newPoll
        ? await pollApi.addPoll(updatedPoll)
        : await pollApi.updatePoll(updatedPoll);
    } catch (e: any) {
      throw new Error(e.message);
    } finally {
      navigate("/");
    }
  };

  /**
   * in edit mode, on mount, pull the poll we're editing from API
   */
  useEffect(() => {
    void (async () => {
      if (!newPoll) await getPollFromAPI();
    })();
  }, []);

  return (
    <>
      {loading ? (
        <div className="flex flex-col gap-4 justify-center items-center my-auto">
          <Loader className="" size={86} color={COLORS["ep-blue"]} />
        </div>
      ) : (
        <div className="flex flex-col items-center w-full justify-center my-auto">
          <form className="w-full lg:w-2/3" onSubmit={form.onSubmit(save)}>
            <div className="glass px-12 py-8">
              <div className="flex items-center">
                <Link to="/">
                  <Button variant="subtle">
                    <IconArrowLeft />
                  </Button>
                </Link>
                <h1 className="text-2xl mb-2 pt-2 mx-2 font-bold">
                  {newPoll ? `New Poll` : `Edit Poll: ${currentPoll?.title}`}
                </h1>
              </div>
              <TextInput
                className="mb-2"
                withAsterisk
                label="Title"
                placeholder="New Poll"
                key={form.key("title")}
                {...form.getInputProps("title")}
              />
              <Textarea
                placeholder="New Description"
                className="mb-2"
                key={form.key("description")}
                label="Description"
                withAsterisk
                {...form.getInputProps("description")}
              />
              {currentPoll?.options.map((option, index) => {
                return (
                  <div
                    key={index}
                    className="flex w-full items-center gap-2 mb-1"
                  >
                    <TextInput
                      className="w-full mb-2"
                      placeholder={`Option ${index + 1}`}
                      withAsterisk={index < 2}
                      label={option.title || `Option ${index + 1}`}
                      key={form.key(`options.${index}.title`)}
                      {...form.getInputProps(`options.${index}.title`)}
                    />
                    {index > 1 && (
                      <Button
                        onClick={() => removeOptionFormFields(index)}
                        variant="danger"
                        size={"sm"}
                        className="mt-4 w-[145px] sm:w-[125px] bg-ep-red"
                        leftSection={<IconX size={20} />}
                      >
                        Remove
                      </Button>
                    )}
                  </div>
                );
              })}
              <div className="mt-4 flex gap-2">
                <Button
                  onClick={() => addOptionFormFields()}
                  type="button"
                  color={COLORS["main"]}
                  className="btn btn-secondary"
                  leftSection={<IconPlus size={20} />}
                >
                  Add Option
                </Button>
                <Button
                  type="submit"
                  color={COLORS["ep-green"]}
                  className="btn bg-ep-green btn-primary"
                  leftSection={<IconCheck size={20} />}
                >
                  Submit
                </Button>
              </div>
            </div>
          </form>
        </div>
      )}
    </>
  );
};
