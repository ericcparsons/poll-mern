import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { PollApi } from "../../api/poll.api";
import { PollOption } from "../pollOption/PollOption";
import { Button, Modal } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { COLORS, IOption, IPoll } from "../../types";
import { IconPencil, IconTrash, IconCheck } from "@tabler/icons-react";

interface PollCardProps {
  poll: IPoll;
  refresh: () => Promise<void>;
}

export const PollCard = ({ poll, refresh }: PollCardProps) => {
  const pollApi = PollApi.getInstance();

  //for the current poll, the current poll voting option that has it's radio button selected
  const [selectedOption, setSelectedOption] = useState("");

  //have we submitted a vote?
  const [voted, setVoted] = useState(false);

  //total of number of votes for all options
  const [totalVotes, setTotalVotes] = useState(0);

  const [opened, { open, close }] = useDisclosure(false);

  /**
   * count the total number of votes amongst all the options
   */
  const countTotalVotes = () => {
    let total = 0;
    poll.options.forEach((option: IOption) => {
      total += option.votes;
    });
    setTotalVotes(total);
  };

  /**
   * send the selected option to the redux state and update (increment) the database
   * */
  const sendOptionVote = async () => {
    try {
      await pollApi
        .incPollOptionVote(poll._id, selectedOption)
        .then(async () => {
          await refresh().then(() => {
            setVoted(true);
            countTotalVotes();
          });
        });
    } catch (e: any) {
      console.error(`Unable to update poll ${poll.title}, ${e}`);
      throw new Error(e.message);
    }
  };

  /**
   * delete the current poll from the db
   * @param id: ObjectID of the poll
   */
  const deletePoll = async () => {
    await pollApi.deletePoll(poll._id).then(async () => {
      close();
      await refresh();
      countTotalVotes();
    });
  };

  /**
   * any time the current poll changes, recount the total number of votes
   */
  useEffect(() => {
    countTotalVotes();
  }, [JSON.stringify(poll)]);

  return (
    <div className="my-4 bg-ep-glass glass p-5 w-full lg:w-2/3">
      <h2 className="font-semibold text-2xl">{poll.title}</h2>
      <h5 className="font-medium italic text-md text-ep-dark-gray">
        {poll.description}
      </h5>
      <div className="flex flex-col justify-start">
        {/* list all of the vote options */}
        {poll.options.map((option, index) => {
          return (
            <PollOption
              key={index}
              setSelectedOption={setSelectedOption}
              pollId={poll._id}
              option={option}
              totalVotes={totalVotes}
              voted={voted}
            />
          );
        })}
      </div>
      <div className="mt-4">
        <div className="flex justify-between">
          {voted ? (
            <Button
              color={COLORS["main"]}
              onClick={async () => {
                setVoted(false);
              }}
              leftSection={<IconCheck size={20} />}
            >
              Vote Again?
            </Button>
          ) : (
            <Button
              color={COLORS["main"]}
              onClick={async () => {
                sendOptionVote();
              }}
              leftSection={<IconCheck size={20} />}
            >
              Vote
            </Button>
          )}

          <div className="flex gap-2">
            <Link to={`/edit/poll?id=${poll._id}`}>
              <Button
                color={COLORS["ep-blue"]}
                leftSection={<IconPencil size={20} />}
              >
                Edit
              </Button>
            </Link>
            <Button
              color={COLORS["ep-red"]}
              leftSection={<IconTrash size={20} />}
              onClick={open}
            >
              Delete
            </Button>
          </div>
        </div>
      </div>
      <Modal
        centered
        opened={opened}
        onClose={close}
        title="Delete"
        size={"md"}
        overlayProps={{
          backgroundOpacity: 0.55,
          blur: 3,
        }}
      >
        <div className="flex text-md">
          Are you sure you want to delete poll “{poll.title}”?
        </div>
        <div className="flex justify-end gap-4 mt-8">
          <Button onClick={close} color={COLORS["main"]}>
            Cancel
          </Button>
          <Button
            onClick={deletePoll}
            color={COLORS["ep-red"]}
            leftSection={<IconTrash size={20} />}
          >
            Yes
          </Button>
        </div>
      </Modal>
    </div>
  );
};
