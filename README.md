# Polls App

Client bootstrapped with create-react-app, Server via express.

Client is hosted on vercel at [https://poll-mern-client.vercel.app/](https://poll-mern-client.vercel.app/)

Server is hosten on render at [https://poll-mern.onrender.com/](https://poll-mern.onrender.com/)
