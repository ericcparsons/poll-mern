"use strict";
/**
 * This page demonstrates how to use interfaces, Models and schmas to effectivley interact with MongoDB
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.PollModel = void 0;
var mongoose_1 = require("mongoose");
var mongoose = require("mongoose");
var base_model_1 = require("./base.model");
var OptionsSchema = new mongoose_1.Schema({
    _id: mongoose_1.Schema.Types.ObjectId,
    title: { type: String, required: true },
    votes: { type: Number, required: true, default: 0 },
});
/*
 * Create the schmema that will reflect the MongoDB collection
 */
var PollSchema = new mongoose_1.Schema({
    title: { type: String, required: true },
    description: { type: String },
    options: [OptionsSchema],
});
/*
 * Use this model class to have acess to common CRUD features
 */
var PollModel = /** @class */ (function (_super) {
    __extends(PollModel, _super);
    function PollModel() {
        return _super.call(this, mongoose.model('Poll', PollSchema)) || this;
    }
    /**
     * Find a Poll in the DB with an option who's title matches optionTitle, and increment the votes by 1
     * @param pollId
     * @param optionTitle
     * @returns
     */
    PollModel.prototype.incOptionVotesById = function (pollId, optionTitle) {
        console.log("mutating the db with a new votes value... pollId: ".concat(pollId, ", optionId:").concat(optionTitle));
        return this.mongooseModel
            .findOneAndUpdate({ _id: pollId, 'options.title': optionTitle }, { $inc: { 'options.$.votes': 1 } })
            .exec();
    };
    return PollModel;
}(base_model_1.BaseModel));
exports.PollModel = PollModel;
