"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.middleware = void 0;
var cors_1 = __importDefault(require("cors"));
var express_1 = __importDefault(require("express"));
var express_session_1 = __importDefault(require("express-session"));
var multer_1 = __importDefault(require("multer"));
exports.middleware = [
    (0, express_session_1.default)({ secret: "poll-mern" }),
    express_1.default.json(),
    express_1.default.urlencoded({ extended: true }),
    (0, multer_1.default)({ dest: process.cwd() + "/tmp" }).single("media"),
    function (req, res, next) {
        res.set("Cache-Control", "no-store, max-age=0");
        next();
    },
    (0, cors_1.default)({
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
        credentials: true,
    }),
];
