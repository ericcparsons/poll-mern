"use strict";
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
var env_1 = require("./environment/env");
var app_1 = require("./app");
var middleware_1 = require("./middleware");
var polls_router_1 = require("./routes/polls.router");
var port = (_a = (0, env_1.env)().port) !== null && _a !== void 0 ? _a : 3000;
var dbConString;
try {
    dbConString = (0, env_1.env)().db.uri();
}
catch (_b) {
    console.log("Failed to create DB Connection string");
}
/**
 * Configure App instance
 */
var app = new app_1.App(port, middleware_1.middleware, [polls_router_1.pollRouter] //* Add your express router objects here
);
/**
 * Connect to MongoDB
 */
if (dbConString) {
    app.mongoDB(dbConString);
}
else {
    console.log("Not Starting MongoDB Connection");
}
/**
 * Launch!
 */
app.listen();
