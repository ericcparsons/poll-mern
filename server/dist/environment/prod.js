"use strict";
module.exports = {
    stage: process.env.ENVIRONMENT,
    port: process.env.PORT,
    domain: "",
    apiPath: "/api/v1",
    staticPath: "public",
    db: {
        uri: function (user, pw, name, account) {
            return process.env.URI;
        },
    },
};
