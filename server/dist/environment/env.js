"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.env = void 0;
var process_1 = require("process");
var env = function () {
    if (process.env.ENVIRONMENT === "dev") {
        var env_1 = require("./dev");
        return env_1;
    }
    else if (process.env.ENVIRONMENT === "production") {
        var env_2 = require("./prod");
        return env_2;
    }
    else {
        console.log("Error. shell variable ENVIRONMENT not set. Acceptable values are 'dev' | 'production'");
        (0, process_1.exit)(1);
    }
};
exports.env = env;
