"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = void 0;
var env_1 = require("./environment/env");
var express = require("express");
var mongoose = require("mongoose");
/**
 * Primary Class that constructs all of the parts of the Express server
 */
var App = /** @class */ (function () {
    /**
     * @param port Port Application listens on
     * @param middleware Array of middleware to be applied to app
     * @param routes Array of express.Router objects for application routes
     * @param apiPath Base path for this api that will be prepended to all routes
     * @param staticPath path to folder for public files express will make available
     */
    function App(port, middleware, routes, apiPath, staticPath) {
        if (apiPath === void 0) { apiPath = (0, env_1.env)().apiPath ? (0, env_1.env)().apiPath : "/api"; }
        if (staticPath === void 0) { staticPath = (0, env_1.env)().staticPath ? (0, env_1.env)().staticPath : "public"; }
        this.port = port;
        this.apiPath = apiPath;
        this.staticPath = staticPath;
        //* Create a new express app
        this.app = express();
        //add the rest of the middleare
        this.middleware(middleware);
        this.routes(routes);
        this.assets(this.staticPath);
    }
    /**
     * @param mware Array of middlewares to be loaded into express app
     */
    App.prototype.middleware = function (mware) {
        var _this = this;
        mware.forEach(function (m) {
            _this.app.use(m);
        });
    };
    App.prototype.addMiddleWare = function (middleWare) {
        this.app.use(middleWare);
    };
    /**
     * Attaches route objects to app, appending routes to `apiPath`
     * @param routes Array of router objects to be attached to the app
     */
    App.prototype.routes = function (routes) {
        var _this = this;
        this.app.get("/", function (req, res) {
            res.send("Welcome To Polls Server!");
        });
        routes.forEach(function (r) {
            _this.app.use("".concat(_this.apiPath), r);
        });
    };
    /**
     * Enable express to serve up static assets
     */
    App.prototype.assets = function (path) {
        this.app.use(express.static(path));
    };
    /**
     * Creates a connection to a MongoDB instance using mongoose
     * @param uri MongoDB connection string
     */
    App.prototype.mongoDB = function (uri) {
        var _this = this;
        var clientOptions = {
            dbName: "polls",
            serverApi: { version: "1", strict: true, deprecationErrors: true },
        };
        var connect = function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, mongoose
                            .connect(uri, clientOptions)
                            .then(function () { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, mongoose.connection.db.admin().command({ ping: 1 })];
                                    case 1:
                                        _a.sent();
                                        console.log("Pinged your deployment.  Successfully connected to MongoDB!");
                                        return [2 /*return*/];
                                }
                            });
                        }); })
                            .catch(function (error) {
                            console.log("DATABASE CONNECTION FAILED \n", error);
                            return process.exit(1);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); };
        connect();
        mongoose.connection.on("disconnected", connect);
    };
    /**
     * Start the Express app
     */
    App.prototype.listen = function () {
        var _this = this;
        this.app.listen(this.port, function () {
            console.log("APP LISTENING ON PORT:", _this.port);
        });
    };
    return App;
}());
exports.App = App;
