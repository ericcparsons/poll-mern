"use strict";
/**
 * Use this module file to create instances of all controllers and simplify imports in to your routers
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.pollController = void 0;
var poll_controller_1 = require("./poll.controller");
exports.pollController = new poll_controller_1.PollController();
