"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseController = void 0;
var env_1 = require("../environment/env");
/**
 * Provides functions to be used with express routes. Serves common CRUD fuctionality.
 */
var BaseController = /** @class */ (function () {
    function BaseController(model) {
        this.useModReturnNew = { useFindAndModify: false, new: true };
        this.model = model;
    }
    /**
     * Sends the document as JSON in the body of response, and sets status to 200
     * @param doc the MongoDB document to be returned to the client as JSON
     * @param res the response object that will be used to send http response
     */
    BaseController.prototype.jsonRes = function (doc, res) {
        res.status(200).json(doc);
    };
    /**
     * @param err error object of any type genereated by the system
     * @param message custom response message to be provided to the client in a JSON body response ({error:'message'})
     * @param res response object to be used to to send
     * @param status custom status code, defaults to 500
     */
    BaseController.prototype.errRes = function (err, res, message, status) {
        if (message === void 0) { message = "Sever Error"; }
        if (status === void 0) { status = 500; }
        if ((0, env_1.env)().stage === "dev") {
            res.status(status).json({ error: message, tsErr: err.message });
        }
        else {
            res.status(status).json({ error: message });
        }
    };
    /**
     * Creates a new document
     */
    BaseController.prototype.create = function (res, document, populate, errMsg) {
        if (errMsg === void 0) { errMsg = "Failed to create"; }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.model
                            .create(document)
                            .then(function (doc) {
                            if (populate) {
                                doc
                                    .populate(populate)
                                    .then(function (populatedDoc) {
                                    _this.jsonRes(populatedDoc, res);
                                })
                                    .catch(function (err) {
                                    _this.errRes(err, res, errMsg);
                                });
                            }
                            else {
                                _this.jsonRes(doc, res);
                            }
                        })
                            .catch(function (err) {
                            _this.errRes(err, res, errMsg);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Returns all documents of model
     */
    BaseController.prototype.find = function (res, populate, errMsg) {
        if (errMsg === void 0) { errMsg = "Failed to find documents"; }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.model.find(populate).then(function (doc) {
                            _this.jsonRes(doc, res);
                        }, function (err) {
                            _this.errRes(err, res, errMsg);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Returns single doucument of model specified by _id.
     */
    BaseController.prototype.findById = function (res, documentId, populate, errMsg) {
        if (errMsg === void 0) { errMsg = "Failed to find document ".concat(documentId); }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.model
                            .findById(documentId, populate)
                            .then(function (doc) {
                            _this.jsonRes(doc, res);
                        }, function (err) {
                            _this.errRes(err, res, errMsg);
                        })
                            .catch(function (err) {
                            _this.errRes(err, res, "Failed to retrieve doc");
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Returns single document from given model that matches the query.
     */
    BaseController.prototype.findOne = function (res, query, populate, errMsg) {
        if (errMsg === void 0) { errMsg = "Failed to find document ".concat(query); }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.model.findOne(query, populate).then(function (doc) {
                            _this.jsonRes(doc, res);
                        }, function (err) {
                            _this.errRes(err, res, errMsg);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    BaseController.prototype.findMany = function (res, query, populate, errMsg) {
        if (errMsg === void 0) { errMsg = "Failed to find document ".concat(query); }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.model.findMany(query, populate).then(function (doc) {
                            _this.jsonRes(doc, res);
                        }, function (err) {
                            _this.errRes(err, res, errMsg);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Updates single document,
     */
    BaseController.prototype.updateById = function (res, documentId, document, populate, errMsg) {
        if (errMsg === void 0) { errMsg = "Failed to update document ".concat(documentId); }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.model.updateById(documentId, document, populate).then(function (doc) {
                            _this.jsonRes(doc, res);
                        }, function (err) {
                            _this.errRes(err, res, errMsg);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Deletes a single document selected by id
     */
    BaseController.prototype.deleteById = function (res, documentId, errMsg) {
        if (errMsg === void 0) { errMsg = "Failed to delete document ".concat(documentId); }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.model.deleteById(documentId).then(function (doc) {
                            _this.jsonRes(doc, res);
                        }, function (err) {
                            _this.errRes(err, res, errMsg);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return BaseController;
}());
exports.BaseController = BaseController;
