import { env } from "./environment/env";
import { App } from "./app";
import { middleware } from "./middleware";
import { pollRouter } from "./routes/polls.router";

const port: number = env().port ?? 3000;
let dbConString;

try {
  dbConString = env().db.uri();
} catch {
  console.log("Failed to create DB Connection string");
}

/**
 * Configure App instance
 */
const app = new App(
  port,
  middleware,
  [pollRouter] //* Add your express router objects here
);

/**
 * Connect to MongoDB
 */
if (dbConString) {
  app.mongoDB(dbConString);
} else {
  console.log("Not Starting MongoDB Connection");
}
/**
 * Launch!
 */
app.listen();
