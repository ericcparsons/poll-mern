import cors from "cors";
import express, { NextFunction, Request, Response } from "express";
import session from "express-session";
import multer from "multer";
export const middleware = [
  session({ secret: "poll-mern" }),
  express.json(),
  express.urlencoded({ extended: true }),
  multer({ dest: process.cwd() + "/tmp" }).single("media"),
  function (req: Request, res: Response, next: NextFunction) {
    res.set("Cache-Control", "no-store, max-age=0");
    next();
  },
  cors({
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    credentials: true,
  }),
];
