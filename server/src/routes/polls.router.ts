//express imports
import express = require("express");
export const pollRouter = express.Router();

//controllers
import { pollController } from "../controllers/controllers.module";

// Set the common part of the path for the routes in this router
const base = "/polls";

//Routes
pollRouter.post(`${base}`, async (req, res) => {
  await pollController.create(res, req.body);
});
pollRouter.put(`${base}/:id`, async (req, res) => {
  await pollController.putPoll(req, res);
});
pollRouter.put(`${base}/option/:id`, async (req, res) => {
  await pollController.putUpdateVote(req, res);
});
pollRouter.delete(`${base}/:id`, async (req, res) => {
  await pollController.deleteById(res, req.params.id);
});
pollRouter.get(`${base}/:id`, async (req, res) => {
  await pollController.findById(res, req.params.id);
});

pollRouter.get(`${base}`, async (req, res) => {
  await pollController.find(res);
});

pollRouter.get("/", async (req, resp) => {
  resp.send("Polls!");
});
