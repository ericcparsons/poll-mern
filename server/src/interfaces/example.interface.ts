import { Document } from 'mongoose';

/**
 * Interfaces for a nested documents within the main schema
 */
interface NestedBase {
  name: string;
}
interface Nested extends NestedBase {
  _id: string;
}
interface NestedDoc extends NestedBase, Document {}
/*
 * Interface with all the document fields, excluding the _id field so it can be extended for multiple uses
 */
interface PollBase<S> {
  title: string;
  description: string;
  options: OptionBase<S>[];
}

interface OptionBase<S> {
  title: string;
  votes: number;
  _id: string;
}
/*
 * Provides type for Docs returned from queries so operations like `set()` or `save()` can be performed
 */

export interface PollDoc extends PollBase<NestedDoc>, Document {}
/*
 * Used as a type for docs sent to API such as on updates or creation
 */

export interface Poll extends PollBase<Nested> {
  _id: string;
}
