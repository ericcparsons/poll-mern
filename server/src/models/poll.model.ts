/**
 * This page demonstrates how to use interfaces, Models and schmas to effectivley interact with MongoDB
 */

import { Schema } from 'mongoose';
import mongoose = require('mongoose');
import { BaseModel } from './base.model';

const OptionsSchema = new Schema({
  _id: Schema.Types.ObjectId,
  title: { type: String, required: true },
  votes: { type: Number, required: true, default: 0 },
});

/*
 * Create the schmema that will reflect the MongoDB collection
 */
const PollSchema: Schema = new Schema({
  title: { type: String, required: true },
  description: { type: String },
  options: [OptionsSchema],
});

/*
 * Use this model class to have acess to common CRUD features
 */
export class PollModel extends BaseModel {
  constructor() {
    super(mongoose.model('Poll', PollSchema));
  }

  /**
   * Find a Poll in the DB with an option who's title matches optionTitle, and increment the votes by 1
   * @param pollId
   * @param optionTitle
   * @returns
   */
  incOptionVotesById<T>(pollId: string, optionTitle: string): Promise<T> {
    console.log(
      `mutating the db with a new votes value... pollId: ${pollId}, optionId:${optionTitle}`
    );

    return this.mongooseModel
      .findOneAndUpdate(
        { _id: pollId, 'options.title': optionTitle },
        { $inc: { 'options.$.votes': 1 } }
      )
      .exec();
  }
}
