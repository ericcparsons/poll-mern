export = {
  stage: process.env.ENVIRONMENT,
  port: process.env.PORT,
  domain: "",
  apiPath: "/api/v1",
  staticPath: "public",
  db: {
    uri: (user?: string, pw?: string, name?: string, account?: string) => {
      return process.env.URI;
    },
  },
};
