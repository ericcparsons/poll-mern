import mongoose = require('mongoose');
import { BaseController } from './base.controller';
import { Response, Request } from 'express';
import { PollModel } from '../models/poll.model';
import { PollDoc, Poll } from '../interfaces/example.interface';

export class PollController extends BaseController {
  model: PollModel;
  constructor() {
    const pollModel = new PollModel();
    super(pollModel);
    this.model = pollModel;
  }

  async putPoll(req: Request, res: Response) {
    try {
      const doc = await this.model.updateById(req.params.id, req.body.poll);
      this.jsonRes(doc, res);
    } catch (e) {
      this.errRes(e, res, 'Failed');
    }
  }

  async putUpdateVote(req: Request, res: Response) {
    try {
      //id of the poll whoss options array we want to search
      const id = req.params.id;
      //title of option we whos votes we want to increment
      const optionTitle = req.query.optionTitle as string;
      const doc = await this.model.incOptionVotesById<PollDoc>(id, optionTitle);
      await doc.save();
      this.jsonRes(doc, res);
      //  poll.options.
    } catch (e) {
      this.errRes(e, res, 'Failed');
    }
  }
}
