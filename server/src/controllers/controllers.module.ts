/**
 * Use this module file to create instances of all controllers and simplify imports in to your routers
 */

import { PollController } from './poll.controller';
export const pollController = new PollController();
